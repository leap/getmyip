package geolocate

import (
	"math"
	"testing"

	"0xacab.org/leap/menshen/pkg/latency"
	"0xacab.org/leap/menshen/pkg/models"
	"github.com/tj/assert"
)

const (
	distanceToleranceKM = 100
)

// getTestLocations returns a a list of given locations
func getTestLocations() []*models.Location {
	return []*models.Location{
		{Label: "nyc", Lat: "40.71", Lon: "-74.01", CountryCode: "US"},
		{Label: "par", Lat: "48.85", Lon: "2.35", CountryCode: "FR"},
		{Label: "stl", Lat: "47.61", Lon: "-122.33", CountryCode: "US"},
		{Label: "ams", Lat: "52.37", Lon: "4.90", CountryCode: "NL"},
		{Label: "mia", Lat: "25.77", Lon: "-80.19", CountryCode: "US"},
		{Label: "mtl", Lat: "45.52", Lon: "-73.65", CountryCode: "CA"},
	}

}

// approxEqual returns true of two magnitudes are approximately equal (up to distanceToleranceKM).
func approxEqual(got, want float64) bool {
	return math.Abs(got-want) < distanceToleranceKM

}

// TestDistance tests the great circule calculations.
// For calibration I'm using this widget in WolframAlpha:
// https://www.wolframalpha.com/widgets/view.jsp?id=577d34bcdd1d8ccc1b3143c93ddd2f6e
func TestDistance(t *testing.T) {
	type args struct {
		from *Point
		to   *Point
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "origin to 90N",
			args: args{
				from: &Point{0, 0},
				to:   &Point{90, 0},
			},
			want: 10002.,
		},
		{
			name: "45N to 45S",
			args: args{
				from: &Point{45, 0},
				to:   &Point{-45, 0},
			},
			want: 9970.,
		},
		{
			name: "ecuator, 0E to 180W",
			args: args{
				from: &Point{0, 0},
				to:   &Point{0, -180},
			},
			want: 20004.,
		},
		{
			name: "pole to pole",
			args: args{
				from: &Point{90, 0},
				to:   &Point{-90, 0},
			},
			want: 20004.,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Distance(tt.args.from, tt.args.to); !approxEqual(got, tt.want) {
				t.Errorf("Distance() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDistanceFromCountry(t *testing.T) {
	type args struct {
		cc string
		to *Point
	}
	tests := []struct {
		name    string
		args    args
		want    float64
		wantErr bool
	}{
		{
			name: "AD to origin",
			args: args{
				"AD", &Point{0, 0},
			},
			want:    4700,
			wantErr: false,
		},
		{
			name: "ad to origin",
			args: args{
				"ad", &Point{0, 0},
			},
			want:    4700,
			wantErr: false,
		},
		{
			name: "NZ to origin",
			args: args{
				"NZ", &Point{0, 0},
			},
			want:    15500,
			wantErr: false,
		},
		{
			name: "TW to origin",
			args: args{
				"TW", &Point{0, 0},
			},
			want:    13100,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DistanceFromCountry(tt.args.cc, tt.args.to)
			if (err != nil) != tt.wantErr {
				t.Errorf("DistanceFromCountry() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !approxEqual(got, tt.want) {
				t.Errorf("DistanceFromCountry() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetNearestCountry(t *testing.T) {
	type args struct {
		from         string
		countryCodes []string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{

		{
			name:    "nearest country to AR is CL",
			args:    args{"AR", []string{"CL", "CA", "DE", "RU"}},
			want:    "CL",
			wantErr: false,
		},
		{
			name:    "nearest country to RU is CN",
			args:    args{"RU", []string{"US", "CA", "VN", "NL", "CN"}},
			want:    "CN",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetNearestCountry(tt.args.from, tt.args.countryCodes)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetNearestCountry() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetNearestCountry() = %v, want %v", got, tt.want)
			}
		})
	}
}

// TestGetEstimatedLatencyBetweenCountries is just testing that the latency dataset is loaded correctly.
// These tests will break when the dataset changes, that's why I've preferred not to encode very precise values.
func TestGetEstimatedLatencyBetweenCountries(t *testing.T) {
	// TODO should probably use a mock here, to decouple tests in this module.
	lm, _ := latency.NewMetric()
	epsylon := 10

	isApprox := func(x float64, y int) bool {
		return int(math.Abs(x-float64(y))) < epsylon
	}

	type args struct {
		lm   *latency.Metric
		from string
		to   string
	}
	tests := []struct {
		name       string
		args       args
		wantApprox int
	}{
		{
			name: "smoke test for known countries",
			args: args{
				lm:   lm,
				from: "CA",
				to:   "FR",
			},
			wantApprox: 120,
		},
		{
			name: "smoke test for unknown countries",
			args: args{
				lm:   lm,
				from: "XX",
				to:   "RU",
			},
			wantApprox: int(math.Inf(1)),
		},
		{
			name: "smoke test for true countries not on the database",
			args: args{
				lm:   lm,
				from: "BO",
				to:   "CA",
			},
			wantApprox: 170,
		},
		{
			name: "AU to US",
			args: args{
				lm:   lm,
				from: "AU",
				to:   "US",
			},
			wantApprox: 230,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetEstimatedLatencyBetweenCountries(tt.args.lm, tt.args.from, tt.args.to); !isApprox(got, tt.wantApprox) {
				t.Errorf("GetEstimatedLatencyBetweenCountries() = %v, want approx %v", got, tt.wantApprox)
			}
		})
	}
}

func TestSortLocationsByDistanceToCountry(t *testing.T) {
	locations := getTestLocations()

	type args struct {
		cc        string
		locations []*models.Location
	}
	tests := []struct {
		name string
		args args
		want []*models.Location
	}{
		{
			name: "sort locations for DE",
			args: args{"DE", locations},
			want: []*models.Location{
				{Label: "ams"},
				{Label: "par"},
				{Label: "mtl"},
			},
		},
		{
			name: "sort locations for de",
			args: args{"de", locations},
			want: []*models.Location{
				{Label: "ams"},
				{Label: "par"},
				{Label: "mtl"},
			},
		},
		{
			name: "sort locations for JP",
			args: args{"JP", locations},
			want: []*models.Location{
				{Label: "stl"},
				{Label: "ams"},
				{Label: "par"},
			},
		},
		{
			name: "sort locations for BR",
			args: args{"BR", locations},
			want: []*models.Location{
				{Label: "mia"},
				{Label: "nyc"},
				{Label: "mtl"},
			},
		},
		{
			name: "sort locations for RU",
			args: args{"RU", locations},
			want: []*models.Location{
				{Label: "ams"},
				{Label: "par"},
				{Label: "stl"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SortLocationsByDistanceToCountry(tt.args.cc, tt.args.locations)
			for i, loc := range got {
				if loc.Label != tt.want[i].Label {
					t.Errorf("SortLocationsByDistanceToCountry()[%d] = %v, want %v", i, loc.Label, tt.want[i].Label)
				}
			}
		})
	}
}

func TestSortLocationsByEstimatedLatencyToCountry(t *testing.T) {
	lm, _ := latency.NewMetric()
	type args struct {
		lm        *latency.Metric
		cc        string
		locations []*models.Location
	}
	tests := []struct {
		name string
		args args
		want []*models.Location
	}{
		{
			name: "get closest locations to DE",
			args: args{
				lm:        lm,
				cc:        "DE",
				locations: getTestLocations(),
			},
			want: []*models.Location{
				{Label: "ams"},
				{Label: "par"},
				{Label: "nyc"},
			},
		},
		{
			name: "get closest locations to AR",
			args: args{
				lm:        lm,
				cc:        "AR",
				locations: getTestLocations(),
			},
			want: []*models.Location{
				{Label: "nyc"},
				{Label: "stl"},
				{Label: "mia"},
			},
		},
		{
			name: "get closest locations to JP",
			args: args{
				lm:        lm,
				cc:        "JP",
				locations: getTestLocations(),
			},
			want: []*models.Location{
				{Label: "mtl"},
				{Label: "nyc"},
				{Label: "stl"},
			},
		},
		{
			name: "get closest locations to BO",
			args: args{
				lm:        lm,
				cc:        "BO",
				locations: getTestLocations(),
			},
			want: []*models.Location{
				{Label: "nyc"},
				{Label: "stl"},
				{Label: "mia"},
			},
		},
		{
			name: "get closest locations to RU",
			args: args{
				lm:        lm,
				cc:        "RU",
				locations: getTestLocations(),
			},
			want: []*models.Location{
				{Label: "ams"},
				{Label: "par"},
				{Label: "nyc"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SortLocationsByEstimatedLatencyToCountry(tt.args.lm, tt.args.cc, tt.args.locations)
			for i, loc := range got {
				if loc.Label != tt.want[i].Label {
					t.Errorf("SortLocationsByEstimatedLatencyToCountry() idx = %d, got %v, want %v", i, loc.Label, tt.want[i].Label)
				}
			}
		})
	}
}

func TestPickBestLocations(t *testing.T) {
	type args struct {
		lm        *latency.Metric
		cc        string
		locations []*models.Location
	}
	tests := []struct {
		name       string
		args       args
		wantLabels []string
	}{
		{
			name: "us is boring",
			args: args{
				lm:        nil,
				cc:        "US",
				locations: getTestLocations(),
			},
			// this is a good place to test: if you're in N and the gateway is S,
			// avoid using it.
			wantLabels: []string{"nyc", "mia"},
		},
		{
			name: "ar",
			args: args{
				lm:        nil,
				cc:        "AR",
				locations: getTestLocations(),
			},
			wantLabels: []string{"nyc", "mia"},
		},
		{
			name: "jp",
			args: args{
				lm:        nil,
				cc:        "JP",
				locations: getTestLocations(),
			},
			wantLabels: []string{"stl", "mtl"},
		},
		{
			name: "cn",
			args: args{
				lm:        nil,
				cc:        "CN",
				locations: getTestLocations(),
			},
			wantLabels: []string{"par", "ams"},
		},
		{
			name: "co",
			args: args{
				lm:        nil,
				cc:        "CO",
				locations: getTestLocations(),
			},
			wantLabels: []string{"mia", "nyc"},
		},
		{
			name: "cu",
			args: args{
				lm:        nil,
				cc:        "CU",
				locations: getTestLocations(),
			},
			wantLabels: []string{"mia", "nyc"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := PickBestLocations(tt.args.lm, tt.args.cc, tt.args.locations)
			gotLabels := []string{}
			for _, e := range got {
				gotLabels = append(gotLabels, e.Label)
			}
			assert.ElementsMatch(t, tt.wantLabels, gotLabels)
		})
	}
}
