// geolocate exposes a function to obtain geographical coordinates for a given country.
// Previously, we used a City location database to obtain a country assignment for the request IP.
// This new approach moves in the direction of delegating the "country" or
// region geolocation to the client side (and let it override when needed), which greatly simplifies
// the responsibility of keeping a geodb up to date.
// The clients are also expected to pass a country code for background queries.
package geolocate
