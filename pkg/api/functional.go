package api

func filter[T any](f func(t T) bool, items []T) []T {
	t := []T{}
	for _, item := range items {
		if f(item) {
			t = append(t, item)
		}
	}
	return t
}

func alltrue[T any](cond []func(T) bool) func(T) bool {
	return func(t T) bool {
		cur := true
		for _, f := range cond {
			cur = f(t) && cur
			if !cur {
				return cur
			}
		}
		return cur
	}
}
