package api

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
)

func storageMiddleware(db *sqlx.DB) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("db", db)
			return next(c)
		}
	}
}

func getDBFromContext(c echo.Context) (*sqlx.DB, error) {
	db := c.Get("db")
	if db == nil {
		return nil, fmt.Errorf("No database found in echo context")
	} else {
		return db.(*sqlx.DB), nil
	}
}
