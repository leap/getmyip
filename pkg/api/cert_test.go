package api

import (
	"encoding/xml"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCertGeneration(t *testing.T) {

	cfg := &Config{
		CaFile:              "../../test/data/ca.crt",
		OvpnCaCrt:           "../../test/data/ovpn_client_ca.crt",
		OvpnCaKey:           "../../test/data/ovpn_client_ca.key",
		OvpnClientCrtExpiry: 1,
		ProviderJson:        "../../test/data/provider.json",
	}

	testTable := []struct {
		name string
		algo string
	}{
		{
			"testing ecdsa",
			"ecdsa",
		},
		{
			"testing ed25519",
			"ed25519",
		},
		{
			"testing rsa",
			"rsa",
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			r, err := newRegistry(cfg)
			require.Nil(t, err, "failed to create registry")
			result, err := r.CertWriter(cfg.OvpnCaCrt, cfg.OvpnCaKey, tc.algo, cfg.OvpnClientCrtExpiry, true)

			assert.Nil(t, err)
			assert.NotNil(t, result)

			var response CertWriterResponse
			err = xml.Unmarshal([]byte(fmt.Sprintf("%s%s%s", "<root>", result, "</root>")), &response)
			assert.Equal(t, nil, err)

			assert.Equal(t,
				len(response.Key) > len("-----BEGIN PRIVATE KEY-----\n-----END PRIVATE KEY-----\n"),
				true,
				fmt.Sprintf("private key seems to be too short %v -> %v", result, response.Key))

			assert.Equal(t,
				len(response.Cert) > len("-----BEGIN CERTIFICATE-----\n-----END CERTIFICATE-----\n"),
				true,
				fmt.Sprintf("VPN certificate seems to be too short %v -> %v", result, response.Key))

			assert.Equal(t,
				len(response.CA) > len("-----BEGIN CERTIFICATE-----\n-----END CERTIFICATE-----\n"),
				true,
				fmt.Sprintf("Root CA cert seems to be too short %v -> %v", result, response.Key))
			t.Logf("CA: %s", response.CA)

		})
	}

}

type CertWriterResponse struct {
	Key  string `xml:"key"`
	Cert string `xml:"cert"`
	CA   string `xml:"ca"`
}
