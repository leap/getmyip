package api

import (
	"net/http"
	"strconv"
	"time"

	m "0xacab.org/leap/menshen/pkg/models"
	"github.com/labstack/echo/v4"
)

// ListAllBridges godoc
// @Summary      Get All Bridges
// @Description  Fetch all bridges. This is an optional API endpoint for compatibility with vpnweb, but do not count on all the providers to have it enabled since it makes it easier to enumerate resources. On the other hand, if the service has "open" VPN endpoints, they can enumerate them here freely. Bridges, however, should be more restricted as a general rule.
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Param        tr	  query  string false "transport (tcp|udp)"
// @Param        port query  string false "port"
// @Param        type query  string false "type"
// @Param        loc  query  string false "location"
// @Success      200  {object}  []models.Bridge
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/bridges [get]
// @Security BucketTokenAuth
func (r *registry) ListAllBridges(c echo.Context) error {
	bridges := []*m.Bridge{}
	for k := range r.bridges {
		bridges = append(bridges, r.bridges[k]...)
	}

	filters := bridgeFiltersFromParams(c, []string{"type"})
	filters = maybeAddBridgeBucketFilter(c, filters)
	filters = maybeAddLastSeenBridgeCutoffFilter(r, filters)
	filtered := filter[*m.Bridge](alltrue(filters), bridges)

	return c.JSON(http.StatusOK, filtered)
}

// I'd love for this to be generic across "endpoints" (ie bridges AND gateways)
// but golang generics/support of sum-types is still quite mediocre :/
// https://github.com/golang/go/issues/48522
// https://github.com/golang/go/issues/51259
// https://github.com/golang/go/issues/57644
func maybeAddBridgeBucketFilter(c echo.Context, filters []func(*m.Bridge) bool) []func(*m.Bridge) bool {
	buckets, ok := c.Get("buckets").([]string)
	if !ok {
		buckets = []string{""}
	}

	return append(filters, func(b *m.Bridge) bool {
		if b.Bucket == "" {
			return true
		}
		for _, bucket := range buckets {
			if b.Bucket == bucket {
				return true
			}
		}
		return false
	})
}

// See comment on maybeAddBridgeBucketFilter for same issue re: making this generic across bridges and gateways
func maybeAddLastSeenBridgeCutoffFilter(r *registry, filters []func(*m.Bridge) bool) []func(*m.Bridge) bool {
	if r.lastSeenCutoffMillis == 0 {
		return filters
	}

	return append(filters, func(b *m.Bridge) bool {
		nowMillis := time.Now().UnixMilli()

		return b.LastSeenMillis >= nowMillis-r.lastSeenCutoffMillis
	})
}

func bridgeFiltersFromParams(c echo.Context, params []string) []func(*m.Bridge) bool {
	filters := make([]func(*m.Bridge) bool, 0)
	for _, param := range params {
		filters = maybeAddBridgeFilter(c, param, filters)
	}
	return filters
}

func maybeAddBridgeFilter(c echo.Context, param string, filters []func(*m.Bridge) bool) []func(*m.Bridge) bool {
	// get the query parameter
	q := c.QueryParam(param)
	if q == "" {
		// empty, return the array of filters
		return filters
	}

	var filter m.EndpointFilter[m.Bridge]
	switch param {
	case "type":
		filter = func(b *m.Bridge) bool {
			return b.Type == q
		}
	case "port":
		port, err := strconv.Atoi(q)
		if err != nil {
			return filters
		}
		filter = func(b *m.Bridge) bool {
			return b.Port == port
		}
	case "loc":
		filter = func(b *m.Bridge) bool {
			return b.Location == q
		}
	default:
		return filters
	}
	return append(filters, filter)
}

// BridgePicker godoc
// @Summary      Get Bridges
// @Description  fetch bridges by location
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Param        location  path      string    true  "Location ID"
// @Success      200  {object}  []models.Bridge
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/bridge/{location} [get]
// @Security BucketTokenAuth
func (r *registry) BridgePicker(c echo.Context) error {
	location := c.Param("location")
	// TODO return error if location not known
	bridges := r.bridges[location]

	filters := make([]func(*m.Bridge) bool, 0)
	filters = maybeAddBridgeBucketFilter(c, filters)
	filters = maybeAddLastSeenBridgeCutoffFilter(r, filters)
	filtered := filter[*m.Bridge](alltrue(filters), bridges)

	return c.JSON(http.StatusOK, filtered)
}
