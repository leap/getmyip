package api

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

const agentSharedKey = "test-key"

func mockOKHandler(c echo.Context) error {
	return c.JSON(http.StatusOK, []string{})
}

func calculateHMAC(body []byte) []byte {
	computed := hmac.New(sha256.New, []byte(agentSharedKey))
	computed.Write(body)
	calculatedHMAC := []byte(computed.Sum(nil))
	return calculatedHMAC
}

func TestAgentRegistrationMiddleware(t *testing.T) {

	testTable := []struct {
		name       string
		genRequest func() ([]byte, *string) // body, HMAC
		expected   func() int
	}{
		{"no hmac provided",
			func() ([]byte, *string) {
				body := []byte("testing-body")
				return body, nil
			},
			func() int {
				return http.StatusUnauthorized
			},
		},
		{"valid hmac provided",
			func() ([]byte, *string) {
				body := []byte("testing-body")
				hmac := calculateHMAC(body)
				hmacString := hex.EncodeToString(hmac)
				return body, &hmacString
			},
			func() int {
				return http.StatusOK
			},
		},
		{"invalid hmac provided",
			func() ([]byte, *string) {
				body := []byte("testing-body")
				hmac := calculateHMAC(body)
				// flip bits of first byte on the hmac
				hmac[0] = hmac[0] ^ 0xff
				hmacString := hex.EncodeToString(hmac)
				return body, &hmacString
			},
			func() int {
				return http.StatusUnauthorized
			},
		},
	}
	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {

			e := echo.New()
			agentEndpoints := e.Group("/agent-auth")
			agentEndpoints.Use(agentRegistrationMiddleware(agentSharedKey))
			agentEndpoints.POST("/test", mockOKHandler)

			requestBody, requestHMAC := tc.genRequest()

			req := httptest.NewRequest(http.MethodPost, "/agent-auth/test", bytes.NewReader(requestBody))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			if requestHMAC != nil {
				req.Header.Set("x-menshen-agent-auth", *requestHMAC)
			}
			rec := httptest.NewRecorder()

			expectedResponse := tc.expected()

			e.ServeHTTP(rec, req)
			assert.Equal(t, expectedResponse, rec.Code)

		})
	}

}
