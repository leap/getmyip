// latency offers a way to lookup distance according to an empirical model of latency.
// This module should be considered experimental, since the dataset it is derived from
// is quite anecdotical. However, it might be the case it throws better a
// better distance metric than the naive spherical distance calculation.
// We're embedding data made public by wonderproxy, from a few hours of crossed pings between their servers:
// https://wonderproxy.com/blog/a-day-in-the-life-of-the-internet/
package latency
