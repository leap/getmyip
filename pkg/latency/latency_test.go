package latency

import (
	"testing"
)

func TestNewLatencyMetric(t *testing.T) {
	tests := []struct {
		name        string
		want        *Metric
		wantNumKeys int
		wantErr     bool
	}{
		{
			name:        "can init ok",
			wantNumKeys: 88, // fucking bad number
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewMetric()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewLatencyMetric() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got.Len() != tt.wantNumKeys {
				t.Errorf("NewLatencyMetric() got num keys = %v, want %v", got.Len(), tt.wantNumKeys)
				return

			}
		})
	}
}
