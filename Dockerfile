FROM debian:12-slim as build
# for installing golang 1.22 from backports
RUN  echo "deb http://deb.debian.org/debian bookworm-backports main"\
  > /etc/apt/sources.list.d/backports.list
RUN apt-get -q update \
  && DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
  ca-certificates \
  build-essential \
  pkg-config \
  golang-1.22-go \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ENV PATH="$PATH:/usr/lib/go-1.22/bin/"
ENV CGO_ENABLED=1
ADD . /src
RUN cd /src && go build ./cmd/menshen && strip menshen && go build ./cmd/invitectl && strip invitectl

FROM debian:12-slim
RUN apt-get -q update \
  && apt-get -qy upgrade \
  && DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
  ca-certificates \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
USER 1008
COPY --from=build /src/menshen /usr/local/bin/menshen
COPY --from=build /src/invitectl /usr/local/bin/invitectl
CMD ["menshen"]
