# countries.csv

Source: https://www.kaggle.com/datasets/paultimothymooney/latitude-and-longitude-for-every-country-and-state?resource=download
License: CC BY-SA 4.0

# regions.tsv
Source: https://github.com/wikimedia-research/canonical-data/blob/master/country/countries.tsv
License: CC-BY-SA (?)
