package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"0xacab.org/leap/menshen/storage"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Show bucket(s) for a supplied invite token",
	Run: func(cmd *cobra.Command, args []string) {
		doCheckInviteToken(cmd)
	},
}

var token string

func init() {
	rootCmd.AddCommand(checkCmd)
	checkCmd.Flags().StringVarP(&token, "token", "t", "", "Invite token you want to check. Use - to enter invite code interactively")
}

func doCheckInviteToken(cmd *cobra.Command) {

	if len(token) == 0 {
		log.Error().Msg("Invite token needs to be specified")
		cmd.Help() // nolint: errcheck
		os.Exit(1)
	}

	if token == "-" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter invite token: ")
		var err error
		token, err = reader.ReadString('\n')
		failOnError("Could not read string from command line", err)
		token = strings.TrimSpace(token)
	}
	checkInviteToken()
}

func checkInviteToken() {
	log.Info().Msgf("Checking invite token %s", token)
	hashedToken := hashInviteToken(token)

	db, err := storage.OpenDatabase(dbPath)
	failOnError("Could not open database", err)
	defer db.Close()

	printBucketsByInviteToken(db, hashedToken)
}
